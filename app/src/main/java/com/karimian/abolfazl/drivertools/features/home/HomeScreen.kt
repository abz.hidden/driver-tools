package com.karimian.abolfazl.drivertools.features.home

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.karimian.abolfazl.drivertools.CustomItem
import com.karimian.abolfazl.drivertools.TestViewModel
import com.karimian.abolfazl.drivertools.model.TestModel
import com.karimian.abolfazl.drivertools.repository.TestRepository
import com.karimian.abolfazl.drivertools.ui.theme.purple

@Composable
fun HomeScreen(viewModel: TestViewModel,navController: NavController) {
    val context = LocalContext.current
    Surface(color = Color.Cyan, modifier = Modifier.fillMaxSize()) {
     //   Column(modifier = Modifier.fillMaxSize()) {

            val testRepository = TestRepository()
            val getMenuItem = testRepository.getMenuItem()
            LazyColumn(contentPadding = PaddingValues(12.dp),
                verticalArrangement = Arrangement.spacedBy(12.dp)){
                items(items = getMenuItem){
                        TestModel -> CustomItem(testModel = TestModel)
                }
            }



//            Box(
//                modifier = Modifier.fillMaxSize(),
//                contentAlignment = Alignment.Center
//            ) {
//                Text(text = viewModel.getHomeScreenText())
//                Button(
//                    onClick = {
//                        Toast.makeText(context,"fuck",Toast.LENGTH_SHORT).show()
//                    },
//                    modifier = Modifier
//                        .align(Alignment.BottomCenter)
//                        .padding(bottom = 30.dp)
//                ) {
//                    Text(text = "Navigate")
//                }
//            }
     //   }
    }
}