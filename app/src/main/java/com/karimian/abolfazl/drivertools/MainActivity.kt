package com.karimian.abolfazl.drivertools

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.ContextCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.karimian.abolfazl.drivertools.features.home.HomeScreen
import com.karimian.abolfazl.drivertools.features.splash.SplashScreen
import com.karimian.abolfazl.drivertools.ui.theme.DriverToolsTheme

class MainActivity : ComponentActivity() {

    private val viewModel : TestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
  //          this.window.statusBarColor = ContextCompat.getColor(this,R.color.white)

            DriverToolsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenMain(viewModel)
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DriverToolsTheme {
        ScreenMain(TestViewModel())
    }
}

@Composable
fun ScreenMain(viewModel: TestViewModel) {

    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Routes.Splash.route) {
        composable(Routes.Splash.route){ SplashScreen(viewModel = viewModel,navController = navController) }
        composable(Routes.Home.route){ HomeScreen(viewModel = viewModel,navController = navController) }
    }
}