package com.karimian.abolfazl.drivertools

sealed class Routes(val route: String) {
    object Home : Routes("home")
    object Splash : Routes("splash")
//    object Settings : Routes("setting")
}
