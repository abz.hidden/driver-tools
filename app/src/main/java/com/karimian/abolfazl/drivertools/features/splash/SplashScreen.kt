package com.karimian.abolfazl.drivertools.features.splash

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.karimian.abolfazl.drivertools.Routes
import com.karimian.abolfazl.drivertools.TestViewModel
import com.karimian.abolfazl.drivertools.ui.theme.purple
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(viewModel: TestViewModel,navController: NavController) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(purple),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = "Driver Tools",
            fontSize = 32.sp,
            modifier = Modifier.wrapContentSize(),
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontStyle = FontStyle.Normal,
        )

        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = "0.0.1",
            fontSize = 16.sp,
            modifier = Modifier.wrapContentSize(),
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontStyle = FontStyle.Normal,
        )
        Spacer(modifier = Modifier.height(10.dp))

        LaunchedEffect(true) {
            delay(2000)
            navController.navigate(Routes.Home.route){
                popUpTo(Routes.Splash.route) {
                    inclusive = true
                }
                // or
                //   popUpTo(0)
            }
        }
    }
}