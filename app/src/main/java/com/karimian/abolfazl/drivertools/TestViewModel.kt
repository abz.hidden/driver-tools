package com.karimian.abolfazl.drivertools

import androidx.lifecycle.ViewModel

class TestViewModel : ViewModel() {

    fun getSplashScreenText() = "This is Splash Screen"
    fun getHomeScreenText() = "This is Home Screen"

}