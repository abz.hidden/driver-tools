package com.karimian.abolfazl.drivertools.model

data class TestModel(
    val id:Int,
    val title:String,
    val text:String,
)
